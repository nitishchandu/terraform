

#ec2 Server Deployment

resource "aws_instance" "serverbuild" {
  ami           = "${var.ami-id}"
  instance_type = "${var.InstanceType}"
  disable_api_termination = true
  key_name = "${var.keyid}"
  subnet_id = "${var.subnetid}"
  iam_instance_profile = "ManagedServices"
  security_groups = ["${var.security-groups}"]
  tags = {
    Name = "${var.instance_name}"
    account_id = "${var.account_id}"
    BILLINGCODE = "${var.Billingcode}"
    MRP = "${var.mrp}"
    ENVIRONMENT  = "${var.environment}"
    REQUEST_ID   = "${var.requestid}"
    BILLINGCONTACT = "${var.billingcontact}"
    CLIENT = "${var.client}"
    CONTACTS = "${var.contact}"
    COUNTRY = "${var.country}"
    CSCLASS = "${var.csclass}"
    CSQUAL = "${var.csqual}"
    CSTYPE = "${var.cstype}"
    FUNCTION = "${var.function}"
    GROUPCONTACT = "${var.groupcontact}"
    PRIMARYCONTACT = "${var.primarycontact}"
    GROUPCONTACT = "${var.secondarycontact}"
    SECONDARYCONTACT = "${var.groupcontact}"
    MEMBERFIRM = "${var.memberfirm}"
    ROLE = "${var.role}"
  }
  root_block_device {
              encrypted = true
              volume_type = "gp2"
              volume_size = "${var.root-size}"
              delete_on_termination = true
			        kms_key_id = "${var.KMSKEYID}"
  }
}
