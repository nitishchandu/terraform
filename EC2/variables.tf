variable "amiid" {
  type = map(string)
  default = {
    "redhat"  = "ami-0f1eb6fdd9d3296ea"
    "ubuntu"  = "ami-0b50942822ae237e8"
    "windows" = "ami-0a27f19efc93edcc8"
  }
}

variable "instancetype" {
  type    = list(string)
  default = ["c5a.large", "c5a.xlarge", "c5a.2xlarge", "db.r5.large"]
}

variable "subnet_id" {
 type    = list(string)
 default = ["subnet-066519ab11d85d771", "subnet-0a20cf598e843191b"]
}

variable "subnetid" {
  type    = map(string)
  default = {
    "0" = "subnet-066519ab11d85d771" #az-1
    "1" = "subnet-0a20cf598e843191b" #az-2
  }
}

variable "securitygroups" {
  type    = string
  default = "sg-05a92598ddd7280b8"
}

variable "keyid" {
  type    = string
  default = "DISOC-key"
}

variable "accountid" {
  type    = string
  default = "764400261212"
}

variable "kmskeyid" {
  type    = string
  default = "arn:aws:kms:us-east-1:764400261212:key/6989d7f9-3cf8-4f27-81aa-108402a6f461"
}

variable "billingcode" {
  type    = string
  default = "FPE03521-SG-SC-DI-3001"
}

variable "mrp" {
  type    = string
  default = "Venkata Sai Nitish Chandu Oggu"
}

variable "requestid" {
  type    = string
  default = "CHG0139846"
}

variable "billingcontact" {
  type    = string
  default = "Ravi Boppe"
}

variable "client" {
  type    = string
  default = "Digital Identity SOC Audit"
}

variable "contact" {
  type    = string
  default = "Naresh Persaud"
}

variable "country" {
  type    = string
  default = "US"
}

variable "csclass" {
  type    = string
  default = "Confidential"
}

variable "csqual" {
  type    = string
  default = "Confidential"
}

variable "cstype" {
  type    = string
  default = "Confidential"
}

variable "function" {
  type    = string
  default = "Consulting"
}

variable "groupcontact" {
  type    = string
  default = "USCIP"
}

variable "primarycontact" {
  type    = string
  default = "Chinonso Kenny"
}

variable "secondarycontact" {
  type    = string
  default = "Pragnya Singhania"
}

variable "memberfirm" {
  type    = string
  default = "US"
}

variable "environment" {
  type    = string
  default = "Production"
}

variable "ec2_root_size" {
  default = "80"
}
