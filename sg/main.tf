provider "aws" {
  profile = var.giver["profile"]
  region  = var.region
}

###########Managed-services-sg###############

resource "aws_security_group" "Managed-services-sg" {
  name        = "Managed-services-sg"
  description = "Managed-services-sg"
  vpc_id      = data.terraform_remote_state.vpc.outputs.vpc-id

  tags = {
    Name = "Terraform-SG"
  }

  ingress {
    from_port   = "445"
    to_port     = "445"
    protocol    = "TCP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "445"
    to_port     = "445"
    protocol    = "UDP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "464"
    to_port     = "464"
    protocol    = "UDP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "464"
    to_port     = "464"
    protocol    = "TCP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "135"
    to_port     = "135"
    protocol    = "UDP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "135"
    to_port     = "135"
    protocol    = "TCP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "389"
    to_port     = "389"
    protocol    = "TCP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "389"
    to_port     = "389"
    protocol    = "UDP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "53"
    to_port     = "53"
    protocol    = "TCP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "53"
    to_port     = "53"
    protocol    = "UDP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "123"
    to_port     = "123"
    protocol    = "TCP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "123"
    to_port     = "123"
    protocol    = "UDP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "3268"
    to_port     = "3268"
    protocol    = "TCP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "88"
    to_port     = "88"
    protocol    = "TCP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "88"
    to_port     = "88"
    protocol    = "UDP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "137"
    to_port     = "137"
    protocol    = "UDP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "137"
    to_port     = "137"
    protocol    = "TCP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "TCP"
    cidr_blocks = ["10.145.126.91/32", "10.145.126.8/32"]
  }

  ingress {
    from_port   = "139"
    to_port     = "139"
    protocol    = "TCP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "636"
    to_port     = "636"
    protocol    = "TCP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "3269"
    to_port     = "3269"
    protocol    = "TCP"
    cidr_blocks = ["10.145.126.64/28", "10.145.126.80/28"]
  }

  ingress {
    from_port   = "4421"
    to_port     = "4421"
    protocol    = "TCP"
    description = "CIP Open VPN"
    cidr_blocks = ["10.145.126.0/24", "172.16.216.0/24"]
  }

  ingress {
    from_port   = "4421"
    to_port     = "4421"
    protocol    = "TCP"
    description = "cip new vpn"
    cidr_blocks = ["23.22.152.177/32"]
  }

  ingress {
    from_port   = "1433"
    to_port     = "1433"
    protocol    = "TCP"
    description = "CIP Open VPN"
    cidr_blocks = ["10.145.126.0/24"]
  }

  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "TCP"
    description = "CIP Open VPN"
    cidr_blocks = ["10.145.126.0/25"]
  }

  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "TCP"
    description = "cip new vpn"
    cidr_blocks = ["23.22.152.177/32"]
  }

  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "TCP"
    description = "DISOC Open VPN"
    cidr_blocks = ["172.16.216.0/24"]
  }

  ingress {
    from_port   = "3389"
    to_port     = "3389"
    protocol    = "TCP"
    description = "CIP Open VPN"
    cidr_blocks = ["10.145.126.0/25", "23.22.152.177/32"]
  }

  ingress {
    from_port   = "0"
    to_port     = "65535"
    protocol    = "TCP"
    description = "CHG0148446"
    cidr_blocks = ["172.16.216.0/24"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Output the security group IDs created above

output "Managed-services-sg" {
  value = aws_security_group.Managed-services-sg.id
}

## Custom SG ##

variable "sg_ports" {
  type        = list(number)
  description = "list of ingress ports"
  default     = [80, 9090, 8080, 4421, 8080, 5005, 1389, 389, 636, 8443, 31389, 27017, 3306, 9200, 464, 88, 137, 139, 135, 5601, 5005, 3000, 5202, 22, 1521, 4422, 4489, 1858, 53, 445, 464, 9389, 1194, 2389, 4636, 1858, 9200, 8090, 9090, 443]
}

resource "aws_security_group" "DISOC-Custom-sg" {
  name        = "DISOC-Custom-sg-CHG0148446"
  description = "DISOC-Custom-sg-CHG0148446"
  vpc_id      = data.terraform_remote_state.vpc.outputs.vpc-id
  tags = {
    Name = "DISOC-Custom-SG-CHG0148446"
  }
  dynamic "ingress" {
    for_each = var.sg_ports
    iterator = port
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "tcp"
      description = "CHG0148446"
      cidr_blocks = ["172.16.216.0/24"]
    }
  }

  ingress {
    from_port   = "1636"
    to_port     = "1636"
    protocol    = "TCP"
    description = "CHG0152280"
    cidr_blocks = ["172.16.216.0/24"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

## Domain Controller SG ##

variable "dc_sg_ports" {
  type        = list(number)
  description = "list of ingress ports"
  default     = [464, 389, 445, 9389, 3268, 88, 137, 636, 3269, 53]
}

variable "openvpn_sg_ports" {
  type        = list(number)
  description = "list of ingress ports"
  default     = [22, 4421]
}

resource "aws_security_group" "SG-DISOC-Domain-Controllers" {
  name        = "SG-DISOC-Domain-Controllers"
  description = "SG-DISOC-Domain-Controllers-CHG0149447"
  vpc_id      = data.terraform_remote_state.vpc.outputs.vpc-id
  tags = {
    Name = "SG-DISOC-Domain-Controllers-CHG0149447"
  }
  dynamic "ingress" {
    for_each = var.dc_sg_ports
    iterator = port
    content {
      from_port         = port.value
      to_port           = port.value
      protocol          = "tcp"
      description       = "CHG0149447"
      security_groups   = ["${aws_security_group.DISOC-Custom-sg.id}"]
    }
  }

  dynamic "ingress" {
    for_each = var.openvpn_sg_ports
    iterator = port
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "tcp"
      description = "CHG0149447"
      cidr_blocks = ["172.16.216.0/24"]
    }
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    description = "CHG0149447"
    self        = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
