provider "aws" {
  profile = var.giver["profile"]
  region  = "${var.region}"
}

##Defining Values

variable "vpc-id" {
        description = "VPC ID of client VPC"
}

variable "vpc-cidr" {
        description = "Client VPC's CIDR"
}


variable "subnet-pvt1" {
        description = "Private 1 Subnet's ID"
}

variable "private-subnet-1-cidr" {
        description = "CIDR of Private subnet 1"
}

variable "subnet-pvt2" {
        description = "Private 2 Subnet's ID"
}

variable "private-subnet-2-cidr" {
        description = "CIDR of Private subnet 2"
}

variable "subnet-pub1" {
        description = "Public 1 Subnet's ID"
}

variable "public-subnet-1-cidr" {
        description = "CIDR of Public subnet 1"
}

variable "subnet-pub2" {
        description = "Public 2 Subnet's ID"
}

variable "public-subnet-2-cidr" {
        description = "CIDR of Public subnet 2"
}


## Output of vpc , subnets and CIDRs

 
output "subnet-pvt1" {
  value = "${var.subnet-pvt1}"
}
output "subnet-pvt2" {
  value = "${var.subnet-pvt2}"
}
output "subnet-pub1" {
  value = "${var.subnet-pub1}"
}
output "subnet-pub2" {
  value = "${var.subnet-pub2}"
}

output "vpc-id" {
  value = "${var.vpc-id}"
}

output "vpc-cidr" {
  value = "${var.vpc-cidr}"
}
output "public-subnet-1-cidr" {
  value = "${var.public-subnet-1-cidr}"
}
output "public-subnet-2-cidr" {
  value = "${var.public-subnet-2-cidr}"
}
output "private-subnet-1-cidr" {
  value = "${var.private-subnet-1-cidr}"
}
output "private-subnet-2-cidr" {
  value = "${var.private-subnet-2-cidr}"
}