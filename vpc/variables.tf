variable "giver" {
  type = map(string)
  default = {
    profile = "default"
  }
}

# AWS region
variable "region" {
    type = "string"
    default = "us-east-1"
    description = "Region of launching infra"
}
