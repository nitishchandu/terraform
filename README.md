<h1>Sequence of deployment </h1>

<h2>1.	Setting up the VPC</h2>

VPC deployment :

* Go to vpc folder run terraform init , plan (Enter all necessary parameters).
* Validate the output of terraform plan in step 5 and run terraform apply.

<i>When you run the template you would not be promoted for Variables to be input while running the script:</i>
   1.	AWS region
   2.	VPC CIDR
   3. VPC ID
   4.	VPC Tag
   5.	Private subnet CIDRs
   6. Private Subnet IDs
   7. Public Subnet IDs
   8.	Public subnet CIDRs


<h2>2. Setting up Security Groups </h2>

<i>While deploying the VPC the default region for the environment is set to the region based on requirement template. If you wish to change this to a different default region you would have to modify the region from the variable.tf file in the sg folder. </i>
   1. This TF template will launch manage servcie security group


<h2>3. Setting up the EC2 instances </h2>    

<i> For running the below template follow the below steps </i>
   1. Open main.tf
   2. Edit instance_name
   3. Edit InstanceType
   4. Edit az ( availability zone)
   5. Edit ami-id
   6. Edit keyid
   7. Edit all tags
   8. Edit root-size
   9. Edit KMSKEYID (encryption key arn)
   10. Edit additional volume

   <i> For launching more servers copy paste the module section ie lines 130 to 158 and edit the above values , make sure you use ami with minimum configuration and with no attached volume.
   <i> Use module singlebuild for VMs with just root volume and use module multibuild for VMs with root volume.


<b> All the statefiles will be created under "statesfiles" directory</b>
